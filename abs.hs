aPlusAbsb x y
  | y > 0 = (+)
  | otherwise = (-)
