import Data.Char

import Text.Parsec

import Text.Parsec.String (Parser)

import Text.Parsec.Language (haskellStyle)

import qualified Text.Parsec.Expr as Ex

import qualified Text.Parsec.Token as Tok

import qualified Data.Map as Map

import qualified Control.Monad.State  as St

import Control.Monad.Writer

import Control.Monad

import Control.Monad.Trans

import System.Console.Haskeline

type Name = String


data Expr
     = Var Name
     | Lit Lit
     | App Expr Expr
     | Lam Name Expr
    deriving (Eq, Show)



data Lit
  = LInt Int
  | LBool Bool
  deriving (Show, Eq, Ord)




lexer :: Tok.TokenParser ()
lexer = Tok.makeTokenParser style
    where ops = ["->","\\","+","*","-","="]
	  names = []
	  style = haskellStyle {Tok.reservedOpNames = ops,
				Tok.reservedNames = names,
				Tok.commentLine = "#"}
  

  
reserved :: String -> Parser ()
reserved = Tok.reserved lexer


  
reservedOp :: String -> Parser ()
reservedOp = Tok.reservedOp lexer



identifier :: Parser String
identifier = Tok.identifier lexer



parens :: Parser a -> Parser a
parens = Tok.parens lexer



contents :: Parser a -> Parser a
contents p = do
  Tok.whiteSpace lexer
  r <- p
  eof
  return r



natural :: Parser Integer
natural = Tok.natural lexer




variable :: Parser Expr
variable = do
  x <- identifier
  return (Var x)



number :: Parser Expr
number = do
  n <- natural
  return (Lit (LInt (fromIntegral n)))



lambda :: Parser Expr
lambda = do
  reservedOp "\\"
  args <- many1 identifier
  reservedOp "."
  body <- expr
  return $ foldr Lam body args

term :: Parser Expr
term =  parens expr
    <|> variable
    <|> number
    <|> lambda

expr :: Parser Expr
expr = do
  es <- many1 term
  return (foldl1 App es)




parseExpr :: String -> Either ParseError Expr
parseExpr input = parse (contents expr) "<stdin>" input



data Value
  = VInt Integer
  | VBool Bool
  | VClosure String Expr (Scope)

type Scope = Map.Map String Value

instance Show Value where
  show (VInt x) = show x
  show (VBool x) = show x
  show VClosure{} = "<<closure>>"




data EvalState = EvalState
  { depth :: Int
  } deriving (Show)




type Step = (Int, Expr)
type Eval a = WriterT [Step] (St.State EvalState) a



inc :: Eval a -> Eval a
inc m = do
  St.modify $ \s -> s { depth = (depth s) + 1 }
  out <- m
  St.modify $ \s -> s { depth = (depth s) - 1 }
  return out



red :: Expr -> Eval ()
red x = do
  d <- St.gets depth
  tell [(d, x)]
  return ()




eval :: Scope -> Expr -> Eval Value
eval env expr = case expr of

  Lit (LInt x) -> do
    return $ VInt (fromIntegral x)

  Lit (LBool x) -> do
    return $ VBool x

  Var x -> do
    red expr
    return $ env Map.! x

  Lam x body -> inc $ do
    return (VClosure x body env)

  App a b -> inc $ do
    x <- eval env a
    red a
    y <- eval env b
    red b
    apply x y

apply :: Value -> Value -> Eval Value
apply (VClosure n e clo) ex = do
  eval (extend clo n ex) e
apply _ _  = error "Tried to apply non-closure"



extend :: Scope -> String -> Value -> Scope
extend env v t = Map.insert v t env



emptyScope :: Scope
emptyScope = Map.empty



runEval :: Expr -> (Value, [Step])
runEval x = St.evalState (runWriterT (eval emptyScope x)) (EvalState 0)



showStep :: (Int, Expr) -> IO ()
showStep (d, x) = putStrLn ((replicate d ' ') ++ "=> " ++ show x)



process :: String -> IO ()
process line = do
  let res = parseExpr line
  case res of
    Left err -> print err
    Right ex -> do
      let (out, ~steps) = runEval ex
      mapM_ showStep steps
      print out



main :: IO ()
main = runInputT defaultSettings loop
  where
  loop = do
    minput <- getInputLine "Untyped> "
    case minput of
      Nothing -> outputStrLn "Goodbye."
      Just input -> (liftIO $ process input) >> loop

