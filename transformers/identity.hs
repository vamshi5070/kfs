newtype Identity a = Identity {runIdentity :: a}
  deriving (Eq,Show)

newtype IdentityT m a = IdentityT {runIdentityT :: m a}
  deriving (Eq,Show)

instance Functor Identity where
  fmap f (Identity a)  = Identity $ f a

instance Applicative Identity where
  pure = Identity
  (<*>) (Identity fa) (Identity a) = Identity $ fa a

instance Monad Identity where
  return = pure
  (>>=) (Identity a) f = f a

instance Functor f => Functor (IdentityT f) where
  fmap f (IdentityT a)  = IdentityT $ fmap f a

instance Applicative f => Applicative (IdentityT f) where
  pure a = IdentityT $ pure a
  (<*>) (IdentityT fa) (IdentityT a) = IdentityT $ fa <*> a

instance Monad f => Monad (IdentityT f) where
  return = pure
  (>>=) (IdentityT ma) f = IdentityT $  do
    a <- ma
    runIdentityT $ f a

