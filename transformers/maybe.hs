newtype MaybeT m a = MaybeT {runMaybeT :: m (Maybe a)}

instance (Functor f) => Functor (MaybeT f) where
  fmap f (MaybeT a) = MaybeT $ (fmap . fmap)  f a

instance (Applicative f) => Applicative (MaybeT f) where
  pure = MaybeT . pure . pure
  (<*>) (MaybeT fa) (MaybeT a) = MaybeT $ (<*>) <$> fa <*> a
  
instance (Monad f) => Monad (MaybeT f) where  
  return = pure
  (>>=) (MaybeT ma) f = MaybeT $ do
    a <- ma
    -- res <- runMaybeT (f a)
    case a of
      Just x -> runMaybeT $ f x
      Nothing -> return Nothing
