newtype EitherT e m a = EitherT {runEitherT :: m (Either e a)}

instance (Functor f) => Functor (EitherT e f) where
  fmap f (EitherT a) = EitherT $ (fmap . fmap)  f a

instance (Applicative f) => Applicative (EitherT e f) where
  pure = EitherT . pure . pure
  (<*>) (EitherT fa) (EitherT a) = EitherT $ (<*>) <$> fa <*> a
  
instance (Monad f) => Monad (EitherT e f) where  
  return = pure
  (>>=) (EitherT ma) f = EitherT $ do
    a <- ma
    case a of
      Left x -> return $ Left x
      Right x -> runEitherT $ f x
--       Just x -> runEitherT $ f x
--       Nothing -> return Nothing

