import System.Random
import System.IO

main :: IO ()
main =  do
  secret_number <- getStdRandom ( randomR (1,10)) :: IO Int
  putStr "Secret number: "
  hFlush stdout
  print secret_number
  loop secret_number 1

loop :: Int -> Int -> IO ()
loop secret_number count = do
  putStrLn "Enter a number: "
  guess <- (read <$> getLine) :: IO Int
  if guess > secret_number
    then do 
      putStrLn "Large"
      loop secret_number (count+1)
    else if guess < secret_number
            then do
              putStrLn "Small"
              loop secret_number (count+1)
                 else do
                  putStrLn "Equal"
                  putStrLn $ "Total number of attempts of get the secret number is " <> show (count)
