module Language (
  knuthStyle
  , knuthDef
  ) where

import Text.Parsec
import Text.Parsec.Token
-- import Text.Parsec.Language

knuthStyle :: LanguageDef st
knuthStyle = emptyDef { 
      commentStart      = "{-"
    , commentEnd        = "-}"
    , commentLine       = "//"
    , nestedComments    = False
    , reservedNames     = []
    , reservedOpNames   = []
    , caseSensitive     = True
    , identStart        = letter <|> char '_'
    , identLetter       = alphaNum <|> oneOf "_'"
}
knuthDef  = knuthStyle {
  reservedOpNames = ["+","/","-","*",";","=","<"]
  ,reservedNames = ["int"]
  }
