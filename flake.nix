{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
          haskell-env = pkgs.haskellPackages.ghcWithHoogle( hp: with hp; [
            hspec
            readline
            # hspec
            # readline
            # haskellPackages.haskeline_0_8_2
            random
           parsec_3_1_15_1
            scotty
            # lens-tutorial
           #monad-loops
	   #      unlit
           #scotty
           #parallel
           #split
           #readline
            #Euterpea
            #HSoM
            # yesod
            # stack2nix          
          ]);
          miscPkgs =  with pkgs;
            [ #cabal-install
              ghcid
              #cabal2nix-unwrapped
              #stack
              haskell-language-server
              hlint
            ];
      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            haskell-env
          ] ++ miscPkgs ;
            };
      });
}
