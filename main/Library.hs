module Library where

import Control.Monad
import Control.Applicative
import Data.Char

newtype Parser  a = Parser {parse :: String -> Maybe( a, String)}

runParser :: Parser a -> String -> a
runParser p str = case parse p str of
  Nothing -> error "Error, parse is not possible"
  Just (x,"") -> x
  _ -> error "Parse halted in between"

failure :: Parser a
failure = Parser . const $ Nothing

instance Functor Parser where
  fmap f p = Parser (\str -> case  parse p str of
                        Nothing -> Nothing
                        Just (x,rest) -> Just (f x,rest)
                        )

instance Applicative Parser where
  pure x = Parser (\str -> Just (x,str))
  (<*>) pf px = Parser (\str -> case parse pf str of
                           Nothing -> Nothing
                           Just (f,rest) -> case parse px rest of
                             Nothing -> Nothing
                             Just (x,rest1) -> Just (f x,rest1)
                       )

instance Monad Parser where
  return = pure
  (>>=) pa f = Parser (\str -> case parse pa str of
                          Nothing -> Nothing
                          Just (x,restr) -> parse (f x) restr
                          )

--monads are fun
option :: Parser a -> Parser a -> Parser a
option p q = Parser (\str -> case parse p str of
                        Nothing -> parse q str
                        Just (x,restr) -> Just (x,restr)
                        )

instance Alternative Parser where
  empty = failure
  (<|>) = option

instance MonadPlus Parser where
  mzero = failure
  mplus = option

item :: Parser Char
item = Parser helper
  where helper ""      = Nothing
        helper (x:xs)  = Just (x,xs)

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = do
  ch <- item
  if p ch
    then return ch
    else failure

oneOf :: [Char] -> Parser Char
oneOf str = satisfy (`elem` str)

char :: Char -> Parser Char
char ch = satisfy (ch ==)

string :: String -> Parser String
string = mapM char
  
spaces :: Parser String   -- removes whitespaces
spaces = many $ oneOf "\n\r "

pretoken :: Parser a -> Parser a
pretoken p = spaces >> p

posToken :: Parser a -> Parser a
posToken p = do
  a <- p
  spaces >> return a
  
intoken :: Parser a -> Parser a
intoken p = do
  spaces 
  a <-  p
  spaces
  return a
