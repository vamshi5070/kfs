module AST where

data Expr =
  Lit Integer
  | Add Expr Expr 
  deriving (Eq,Show)
