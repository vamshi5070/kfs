* AST
** Module
#+begin_src haskell :tangle AST.hs
module AST where
#+end_src
** Expr
#+begin_src haskell :tangle AST.hs
  data Expr =
    Int
    | Add Expr Expr 
    deriving (Eq,Show)
#+end_src

* Language
** Module
#+begin_src haskell :tangle Language.hs
module Language where
#+end_src
** Imports
*** Parsec
Letter,alphaNum,char,oneOf
#+begin_src haskell :tangle Language.hs
import Text.Parsec
#+end_src
*** Token
Various definitions like commentStart...
#+begin_src haskell :tangle Language.hs
import Text.Parsec.Token
#+end_src

*** Language
LanguageDef,emptyDef
#+begin_src haskell :tangle Language.hs
import Text.Parsec.Language (LanguageDef ,emptyDef)
#+end_src
** Langstyle
#+begin_src haskell :tangle Language.hs
langStyle :: LanguageDef ()
langStyle = emptyDef {
  commentStart = "{-"
  ,commentEnd = "-}"
  ,commentLine = "--"
  ,nestedComments = False
  ,reservedNames = []
  ,reservedOpNames = []
  ,caseSensitive = True
  ,identStart = letter <|> char '_'
  ,identLetter = alphaNum <|> oneOf "_'"
}
#+end_src

* Parser
** Module
#+begin_src haskell :tangle Parser.hs
module Parser where
#+end_src

** Imports
*** Parsec
Letter,alphaNum,char,oneOf
#+begin_src haskell :tangle Parser.hs 
import Text.Parsec
#+end_src
*** Token
Various definitions like commentStart...
#+begin_src haskell :tangle Parser.hs 
import Text.Parsec.Token as Tok
#+end_src

*** Language
LanguageDef,emptyDef
#+begin_src haskell :tangle Parser.hs 
import Text.Parsec.String (Parser)
#+end_src

*** AST
#+begin_src haskell :tangle Parser.hs
import AST
#+end_src
*** Language
#+begin_src haskell :tangle Parser.hs
import Language
#+end_src
** Lexer
#+begin_src haskell :tangle Parser.hs
lexer :: Tok.TokenParser ()
lexer = Tok.makeTokenParser langStyle
#+end_src
** Number
#+begin_src haskell :tangle Parser.hs
number :: Parser Integer
number = Tok.integer lexer
#+end_src
