module Language where

import Text.Parsec

import Text.Parsec.Token

import Text.Parsec.Language (LanguageDef ,emptyDef)

langStyle :: LanguageDef ()
langStyle = emptyDef {
  commentStart = "{-"
  ,commentEnd = "-}"
  ,commentLine = "--"
  ,nestedComments = False
  ,reservedNames = []
  ,reservedOpNames = ["+"]
  ,caseSensitive = True
  ,identStart = letter <|> char '_'
  ,identLetter = alphaNum <|> oneOf "_'"
}
