module Parser where

import Text.Parsec
import Text.Parsec.Token as Tok
import Text.Parsec.String (Parser)

import Data.Functor.Identity
  
import AST
import Language(langStyle)

lexer :: Tok.TokenParser ()
lexer = Tok.makeTokenParser langStyle

number :: Parser Integer
number = Tok.integer lexer

numberExpr :: Parser Expr
numberExpr = Lit <$> number

reserved :: String -> Parser ()       
reserved = Tok.reserved lexer
  
reservedOp' :: String -> Parser ()
reservedOp' = Tok.reservedOp lexer
  
add :: Parser Expr
add = do
  one <- run
  reservedOp' "+"
  two <-  run
  return $ Add one two
  -- expr <- run  -- <|> numberExpr
  -- return $ Add one expr

run =  numberExpr <|>  add
  
contents :: Parser a -> Parser a
contents p = do
  Tok.whiteSpace lexer
  r <- p
  eof
  return r

parseExpr :: String -> Either ParseError Expr
parseExpr s = parse (contents (run)) "<stdin>" s
  
main = do
  case (parse (contents (run)) "not possible to parse" "1+7") of
    Left err -> print err
    Right xs -> print xs
