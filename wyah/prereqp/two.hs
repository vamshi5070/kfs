{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Control.Monad.Trans
import Control.Monad.State
import Control.Monad.Writer

newtype Stack a = Stack {unStack :: StateT Int (WriterT [Int] IO) a}
  deriving (Monad,MonadState Int,MonadWriter [Int],MonadIO)

-- foo :: Stack ()
-- foo = Stack $ do
--   put 1
--   lift $ tell [2]
--   lift $ tell [23]
--   lift $ lift $ print 43
--   return ()

-- evalStack :: Stack a -> IO [Int]
-- evalStack m = execWriterT (evalStateT (unStack m) 0)
