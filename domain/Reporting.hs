module Reporting where

import Data.Monoid (getSum)

import qualified Database as DB

import Project

data Report = Report
 {
  budgetProfit :: Money
  ,netProfit :: Money
  ,difference :: Money
  } deriving (Eq,Show)

calculateReport :: Budget -> [Transaction] -> Report
calculateReport budget transactions = Report {
  budgetProfit  = budgetProfit'
  ,netProfit = netProfit'
  ,difference = netProfit' - budgetProfit'
  }
  where 
    budgetProfit' = budgetIncome budget - budgetExpenditure budget
    netProfit' =  getSum $ foldMap asProfit transactions
    asProfit (Sale m) = return m
    asProfit (Purchase m) = return $ negate m
