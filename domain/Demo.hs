{-# LANGUAGE OverloadedStrings #-}

module Demo where

import Project

someProject :: Project
someProject = ProjectGroup "Sweden" [stockholm, gutenburg, malmo]
  where
    stockholm = Project 1 "Stockholm"
    gutenburg = Project 2 "Gutenburg"
    malmo = ProjectGroup "Malmo" [city, limhamn]
    city = Project 3 "Malmo city"
    limhamn = Project 4 "Limhamn"
