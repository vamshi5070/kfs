newtype Writer w a = Writer {runWriter :: w -> (a,w) }

instance Functor (Writer w) where
  fmap f (Writer w) = Writer (\x -> let (w x) in
                                 (fa,w'))
one = let x=1 in
  x+1
