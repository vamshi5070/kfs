newtype State s a = State { runState :: s -> (a,s)}

newtype StateT s m a = StateT { runStateT :: s -> m (a,s)}

instance Functor (State s) where
  fmap f (State s) = State (\x -> let (a,s') = s x in
                              (f a,s'))

instance Applicative (State s) where
  pure a = State (\s -> (a,s))
  (<*>) (State fs) (State as) = State (\x -> let (f,s') = fs x
                                                 (a,s'') = as s'
                                             in
                                    (f a,s''))

instance Monad (State s) where
  return = pure
  (>>=) (State sa) f = State (\x -> let (a,s)  =  sa x in
                                          runState (f a) s)

instance (Functor f) =>  Functor (StateT r f) where
  fmap f (StateT sma) = StateT $ (\x -> let abstract =  sma x
                                       in fmap (\(a,s') -> (f a,s')) abstract)

instance (Functor f,Monad f) =>  Applicative (StateT s f) where
  pure a = StateT $ (\s -> pure (a,s))
  (<*>) (StateT fs) (StateT as) = StateT $ \s -> do  
    (f,s') <- fs s
    (a,s'') <- as s'
    return $ (f a,s'')
  
instance (Monad f) =>  Monad (StateT s f) where
  return = pure
  (>>=) (StateT ma) f = StateT $ \s -> do
    (a,s') <- ma s
    runStateT (f a) s

state = 1
