import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader

embedded :: MaybeT (ExceptT String (ReaderT () IO)) Int
-- embedded :: ReaderT () IO (Either a (Maybe Integer))
-- embedded :: ExceptT String (ReaderT () IO) (Maybe Integer)
embedded =MaybeT $ ExceptT $   ReaderT (\() -> do
                       return ((const . Right . Just $ 1) ()) )

one :: Float -> Int -> Int
one = undefined 

main = return ()
