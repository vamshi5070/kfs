import Data.Monoid

newtype Writer w a = Writer { runWriter ::  (a,w)}

newtype WriterT w m a = WriterT { runWriterT :: m (a,w)}

instance Functor (Writer w) where
   fmap f (Writer wa) =  Writer ( let (a,w') =  wa  in
                                    (f a,w'))

instance (Monoid w) => Applicative (Writer w) where
  pure a = Writer (a,mempty)
  (<*>) (Writer fw) (Writer aw) = Writer (let (f,w') = fw
                                              (a,w'') = aw
                                         in (f a , w' <> w''))

instance (Monoid w) => Monad (Writer w) where
  return = pure
  (>>=) (Writer wa) f = Writer (let (a,w) = wa
                                    (b,w') = runWriter $ f a in
                                    (b , w <> w'))

instance (Functor f) => Functor (WriterT w f) where
  fmap f (WriterT wma) = WriterT $ ( let abstract =  wma
                                       in fmap (\(a,s') -> (f a,s')) abstract)

instance (Monad f,Monoid w) => Applicative (WriterT w f) where
  pure a = WriterT $ pure (a,mempty)
  (<*>) (WriterT fw) (WriterT aw) = WriterT $ do
                                                (f,w') <- fw
                                                (a,w'') <- aw
                                                return (f a , w' <> w'')

instance (Monad f,Monoid w) => Monad (WriterT w f) where
  return = pure
  (>>=) (WriterT wa) f = WriterT $ do
    (a,w) <- wa
    runWriterT (f a)
    
