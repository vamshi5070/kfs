data Reader r a = Reader {runReader :: r -> a}

data ReaderT r m a = ReaderT {runReaderT :: r -> m a }

instance Functor (Reader r) where
  fmap f (Reader ra) = Reader $ f . ra 

instance Applicative (Reader r) where
  pure a r = Reader r r
  (<*>) (Reader rab) (Reader ra) = Reader $ \x -> (rab x) (ra x)

instance Monad (Reader r) where
  return  = pure
  (>>=) (Reader ra) f = Reader (\x -> runReader (f $ ra x) x)

instance (Functor f) =>  Functor (ReaderT r f) where
  fmap f (ReaderT rma) = ReaderT $ (fmap . fmap ) f rma
  
instance (Applicative f) => Applicative (ReaderT r f) where
  pure = ReaderT . pure . pure
  (<*>) (ReaderT fa) (ReaderT a) = ReaderT $ (<*>) <$> fa <*> a

instance (Monad f) => Monad (ReaderT r f) where  
  return = pure
  (>>=) (ReaderT ma) f = ReaderT $ \x -> do
    ra <- ma x
    runReaderT (f ra) x
    -- return $ Reader (\x -> runReaderT (f $ ra x) x) 

get :: Reader a a
get = Reader id

-- gets :: Reader 

-- gets :: Reader a a
gets :: (r -> a) -> Reader r a
gets  = Reader

ex :: Reader Int Int
ex = Reader (\x -> x+1) >>= (\a-> Reader (\x -> x+2+a))
  
ex1 :: Reader Int Int
ex1 = do
  a <- gets (+1) --Reader (\x -> x+1)
  y <- gets (\x-> x+2+a)
  return y
