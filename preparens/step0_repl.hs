import System.IO (hFlush, stdout)
-- import Readline (addHistory, readline, load_history)

type KfsVal = String

kfs_read :: String -> KfsVal
kfs_read = id

eval :: KfsVal -> KfsVal
eval = id

kfs_print :: KfsVal -> String
kfs_print = id

rep :: String -> String
rep = kfs_read . eval . kfs_print

repl_loop :: IO ()
repl_loop = do
  putStr "rgv> "
  hFlush stdout
  line <- getLine
  case line of
    ":q" -> return ()
    str -> do
      putStrLn $ rep str    
      hFlush stdout
      repl_loop

main = repl_loop
