module Lexer (parseExpr)
 where

import AST
import Language (langStyle)
import Text.Parsec
import Text.Parsec.String (Parser)
import qualified Text.Parsec.Token as Tok

lexer :: Tok.TokenParser st            -- Defining a lexer with
lexer = Tok.makeTokenParser langStyle  -- langStyle from Language module
  
identifier :: Parser String              -- Identifier which starts with a letter or _
identifier = Tok.identifier lexer        -- Identifer consists of body of 'a'..'z',1..9 or _'

-- parens :: Parser String
parens = Tok.parens lexer

integer :: Parser Integer            -- Integer lexer
integer = Tok.integer lexer
  
-- if/then/else
ifthen :: Parser Expr              -- If then else
ifthen = do
  reserved "if"
  cond <- run -- true <|> false
  reservedOp "then"
  tr <- run -- true <|> false
  reserved "else"
  fl <- run --false <|> true
  return $ If cond tr fl
  
isTrue :: Parser Expr
isTrue = do
  reserved "isTrue"
  predicate <- run --true <|> false
  return $ IsTrue predicate

isZero :: Parser Expr
isZero = do
  reserved "isZero"
  predicate <- run --true <|> false
  return $ IsZero predicate
  
pred' = do
  reserved "pred"
  num <- run
  return $ Pred num
    
succ' = do
  reserved "succ"
  num <- run
  return $ Succ num
  
reserved :: String -> Parser ()       
reserved = Tok.reserved lexer
  
reservedOp :: String -> Parser ()
reservedOp = Tok.reservedOp lexer

whiteSpace = Tok.whiteSpace lexer
  
contents :: Parser a -> Parser a
contents p = do
  Tok.whiteSpace lexer
  r <- p
  eof
  return r

true, false :: Parser Expr
true  = reserved "True"  >> return True'
false = reserved "False" >> return False'
zero = reserved "0" >> return Zero

run = true
  <|> false
  <|> zero
  <|> ifthen
  <|> isTrue
  <|> pred'
  <|> succ'
  <|> isZero
  <|> parens run

parseExpr :: String -> Either ParseError Expr
parseExpr s = parse (contents (run)) "<stdin>" s
  
main = do
  case (parse ifthen "not possible to parse" "if False then True else if True then False else True") of
    Left err -> print err
    Right xs -> print xs
