import AST 
import Lexer
-- import Pretty
  
import Data.Maybe
import Control.Monad.Trans
import System.Console.Haskeline

eval :: Expr -> Maybe Expr
eval (If True' c _) = Just c
eval (If False'  _ c) = Just c
eval (If t c a) = (\t' -> If t' c a) <$> eval t
eval (IsTrue True') = Just True'
eval (IsTrue False') = Just False'
eval (IsZero Zero) = Just True'
eval (IsZero (Succ t)) | isNum t = Just False'
eval (IsZero t) = IsZero <$> (eval t)                       
eval (Pred Zero) = Just Zero
eval (Pred (Succ t)) | isNum t = Just t
eval (Pred t) = Pred <$> eval t
eval (Succ t) = Succ <$> eval t
eval (IsTrue c) = (\x -> IsTrue x) <$> eval c  -- the possiblity of ifthenelse
eval _  = Nothing

nf :: Expr -> Expr
nf x = fromMaybe x (nf <$> eval x)
  
eval' :: Expr -> Maybe Expr
eval' t = case nf t of
  nft | isVal nft -> Just nft
      | otherwise -> Nothing -- term is "stuck"

isNum :: Expr -> Bool
isNum Zero = True
isNum (Succ c) = isNum c
isNum _ = False
  
isVal :: Expr -> Bool
isVal True' = True
isVal False' = True
isVal t | isNum t = True
isVal _ = False
  
process :: String -> IO ()
process line = do
  let res = parseExpr line
  case res of
    Left err -> print err
    Right ex -> case eval' ex of
      Nothing -> putStrLn "Cannot evaluate"
      Just result -> putStrLn $ show result

main :: IO ()
main = runInputT defaultSettings loop
  where
  loop = do
    minput <- getInputLine "Arith> "
    case minput of
      Nothing -> outputStrLn "Goodbye."
      Just ":q" -> outputStrLn "Goodbye."
      Just input -> (liftIO $ process input) >> loop
