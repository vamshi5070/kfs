module AST where

data Expr =          -- An expression could be True,
  True'              -- False or If cond trueValue falseValue
  | False'
  | Zero
  | If Expr Expr Expr
  | IsTrue Expr
  | IsZero Expr
  | Pred Expr
  | Succ Expr
  deriving Show

rgv :: Expr -> String
rgv _ = "rg"
