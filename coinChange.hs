one = 1

firstDenomination :: Int -> Int
firstDenomination 1 = 1
firstDenomination 2 = 5
firstDenomination 3 = 10
firstDenomination 4 = 25
firstDenomination 5 = 50

cc :: Int -> Int -> Int
cc amt kindsOfCoins
        | amt == 0 = 1
        | amt < 0 || kindsOfCoins == 0 = 0
        | otherwise = cc amt (kindsOfCoins - 1) + cc (amt - (firstDenomination kindsOfCoins)) kindsOfCoins


countChange amt = cc amt 5
