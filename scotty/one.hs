{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Control.Monad.Trans
import Data.Monoid (mconcat)

main = scotty 3000 $ do
  get "/:word" $ do
    beam <- param "word"
    lift ( putStrLn "hello")
    html $ mconcat ["<h1>Scotty rgv " , beam , " me up </h1>"]
