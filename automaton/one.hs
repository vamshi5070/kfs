-- an automata that ends with b

initial = 0  -- initial state 

final 1 = False -- final state
final 0 = True


--delta is a transition function
delta 0 'a' = 0
delta 0 'b' = 1
delta 1 'a' = 0
delta 1 'b' = 1
