import Data.List(group,sort,nub)

parse :: [String] -> [[Int]]
parse = map (map (read :: String -> Int) . words ) . drop 1

translate :: [[Int]] -> [(Int,[Int])]
translate [] = []
translate ((_:x:[]):xs:xss) = (x,xs):translate xss
-- translate = foldr (\[] acc -> 
-- main = pa

filter' :: (Int,[Int]) -> [Int]
filter' (n,xs) = nub . filter  (flip elem $ v) $ xs
  where v = valid n xs

valid :: Int -> [Int] -> [Int]
valid n  = map fst  .  filter (\(_,cnt) -> cnt >= n) .  map (\l@(x:_) -> (x,length l)) . group . sort

ls :: [Int]
ls= [4,5,2,5,4,3,1,3,4]

write :: [Int] -> String
write [] = "-1"
write xs = unwords . (map show) $ xs


main = interact $ unlines . map write . (map filter') . translate . parse . lines
rgv = "mass"
rgv' = 4+1

 
