* Lists and GCD
** Test1
Strings with numbers can be compared (Ord typeclass)
#+begin_src haskell
"4" > "2"
#+end_src

#+RESULTS:
: True

** Pairwith
#+begin_src haskell
    :{
    pairWith ::  (a -> a -> b) -> [a]  -> [b]
    pairWith _ []   = []
    pairWith _ [x]  = error "error, only one"
    pairWith f (x:y:xs)  = (f x y):pairWith f xs 
      :}
  pairWith (,) [1,2,3,4] 
    #+end_src

    #+RESULTS:
    | 1 | 2 |
    | 3 | 4 |

** Main
#+begin_src haskell
  :{
mian =  unwords  . foldr1 gcd' . map (pairWith (,)  . words ) . drop 1 . lines
    :}
mian "2\n 7 2\n 2 2 7 1\n"

    #+end_src

    #+RESULTS:
    : unwords :: [String] -> String
    
** GCD for two
#+begin_src haskell
      :{
    gcd' :: [(String,String)] -> [(String,String)] -> [(String,String)]
    -- gcd' []     ys = ys
    -- gcd' xs     [] = xs
    -- gcd' ((x,y):xs) ((x',y'):ys) =
    gcd' xs ys = helper xs ys []
       where helper [] ys zs = zs
	     helper xs [] zs = zs
	     helper ((x,y):xs) ((x',y'):ys) zs
	      | x == x' = helper xs ys ((x,min y y'):zs)
	      | otherwise = helper ((x,y):xs) ys zs -- ((x,min y y'):zs)
	:}
  gcd' [("7","2")] [("2","2"),("7","1")]
       
    #+end_src

    #+RESULTS:
    | 7 | 1 |

** Alternative solution
#+begin_src haskell
  :{
func one two = one ++ two
xmonad = "true"  
    :}
func "rgv" " ass" 
xmonad
  
#+end_src

    #+RESULTS:
    : true

