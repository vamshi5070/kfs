-- k regular
vertices n =  [0..n-1]

graph n = (vertices n,edges n)


edges n = [(i,i+1) | i <- [0..n-2] ++ [n-1,0]] ++ [(i,i+ n `div` 2) | i <- [0..n `div` 2 - 1]]
