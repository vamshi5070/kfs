module AST where

type Name = String

lam = Lam "a" (Lit (LInt 3))
lam1 = Lam "one" (Var "two")

data Expr
  = Var Name
  | Lit Lit
  | App Expr Expr
  | Lam Name Expr
  deriving (Eq, Show)

data Lit
  = LInt Int
  | LBool Bool
  deriving (Show ,Eq,Ord)
