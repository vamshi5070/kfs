one = "one"

data Optional a = None | Only a
  deriving (Eq,Show)

instance Functor Optional where
  fmap _ None = None
  fmap f (Only a) = Only $ f a
