import System.Random
import System.IO

main :: IO ()
main = do
  secret_number <- getStdRandom ( randomR (1,10)) :: IO Int
  putStr "Secret number: "
  hFlush stdout
  print secret_number
  putStr "Guess a number: "
  guess <- (read <$> getLine) :: IO Int
  if guess > secret_number
    then putStr "Large"
    else if guess < secret_number
            then putStr "Small"
                 else putStr "Equal"
  putStrLn ""
  
  
